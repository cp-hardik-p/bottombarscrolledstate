package com.example.demobottombarscrollstate

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.demobottombarscrollstate.model.Plant
import com.example.demobottombarscrollstate.model.plantItems

@Composable
fun PlantItem(item: Plant) {

    Box(
        modifier = Modifier
            .clickable {
            }
            .fillMaxWidth()
            .padding(horizontal = 8.dp, vertical = 4.dp)
            .height(100.dp)
            .background(color = Color.Gray)
    ) {
        Row(
            modifier = Modifier
                .padding(horizontal = 8.dp)
                .fillMaxWidth()

        ) {
            Image(
                painter = painterResource(id = item.imageRes),
                contentDescription = "user icon",
                modifier = Modifier
                    .padding(horizontal = 8.dp)
                    .fillMaxWidth(0.3f)
                    .align(Alignment.CenterVertically)
            )
            Text(
                modifier = Modifier
                    .padding(horizontal = 16.dp)
                    .fillMaxWidth(0.7f)
                    .align(Alignment.CenterVertically),
                text = item.name,
                color = Color.White,
                fontSize = 16.sp
            )
        }
    }
}

@Preview
@Composable
fun PreviewPlantItem(){
    PlantItem(item = plantItems[0])
}