package com.example.demobottombarscrollstate

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.demobottombarscrollstate.model.Plant
import com.example.demobottombarscrollstate.model.Puppy
import com.example.demobottombarscrollstate.model.plantItems
import com.example.demobottombarscrollstate.model.puppyItems
import com.example.demobottombarscrollstate.ui.theme.DemoBottomBarScrollStateTheme
import com.example.demobottombarscrollstate.ui.theme.bottomBarColor
import com.example.demobottombarscrollstate.ui.theme.contentColor

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            DemoBottomBarScrollStateTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    DisplayData(plantItems = plantItems, puppyItems = puppyItems)
                }
            }
        }
    }
}

@Composable
fun DisplayData(plantItems: List<Plant>, puppyItems: List<Puppy>) {
    val navController = rememberNavController()
    val navBackStackEntry = navController.currentBackStackEntryAsState().value
    val currentRoute = navBackStackEntry?.destination?.route

    Scaffold(bottomBar = {
        BottomAppBar(
            contentColor = contentColor,
            backgroundColor = bottomBarColor
        ) {
            NavItem(
                screen = Screen.PlantScreen,
                currentRoute == Screen.PlantScreen.route,
                navController
            )

            NavItem(
                screen = Screen.DoggyScreen,
                currentRoute == Screen.DoggyScreen.route,
                navController
            )

            NavItem(
                screen = Screen.SettingsScreen,
                currentRoute == Screen.SettingsScreen.route,
                navController
            )
        }
    }) {
        NavHost(navController = navController, startDestination = Screen.PlantScreen.route) {
            composable(Screen.PlantScreen.route) {
                LazyColumn(modifier = Modifier.fillMaxHeight()) {
                    items(plantItems) { item ->
                        PlantItem(item = item)
                    }
                }
            }

            composable(Screen.DoggyScreen.route) {
                LazyColumn(modifier = Modifier.fillMaxHeight()) {
                    items(puppyItems) { puppy ->
                        PuppyItem(puppy = puppy)
                    }
                }
            }

            composable(Screen.SettingsScreen.route) {
                Settings()
            }
        }
    }
}

@Composable
fun RowScope.NavItem(screen: Screen, isSelected: Boolean, navController: NavController) {
    BottomNavigationItem(selected = isSelected, label = {
        Text(text = screen.text)
    }, onClick = {
        navController.navigate(screen.route) {
            navController.graph.startDestinationRoute?.let { route ->
                popUpTo(route) {
                    saveState = true
                }
                launchSingleTop = true
                restoreState = true
            }
        }
    }, icon = {})
}

sealed class Screen(val route: String, val text: String) {
    object PlantScreen : Screen("PlantScreen", "Plant")
    object DoggyScreen : Screen("DoggyScreen", "Doggy")
    object SettingsScreen : Screen("SettingsScreen", "Settings")
}





