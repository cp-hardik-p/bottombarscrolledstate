package com.example.demobottombarscrollstate.model

data class Plant(
    val id: Int,
    val name: String,
    val description: String,
    val imageRes: Int
)
